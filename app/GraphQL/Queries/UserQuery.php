<?php
namespace App\GraphQL\Queries;

use App\Models\Note;
use App\Models\User;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class UserQuery extends Query
{
    protected $attributes = [
        'name' => 'Users query'
    ];

    public function type()
    {
        return GraphQL::type('user');
    }

    public function args()
    {
        return [];
    }

    /**
     * @param $root
     * @param $args
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        return auth()->user();
    }
}