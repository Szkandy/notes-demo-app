<?php

namespace App\GraphQL\Types;

use App\Models\Note;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NoteType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Note',
        'description'   => 'A note',
        'model'         => Note::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of a note',
            ],
            'position' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The position of a note',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of a note',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of a note',
            ],
            'color' => [
                'type' => Type::string(),
                'description' => 'The color of a note',
            ],
        ];
    }

}