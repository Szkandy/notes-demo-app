<?php

namespace App\GraphQL\Mutations\Notes;


use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class UpdateNoteMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'updateNote'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return \Graphql::type('note');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id'          => [
                'name'  => 'id',
                'type'  => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
            'title'       => [
                'name' => 'title',
                'type' => Type::string(),
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
            ],
            'color'       => [
                'name'  => 'color',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        $user = auth()->user();
        $note = $user->notes()->find($args['id']);

        $note->update([
            'title'       => $args['title'] ?? '',
            'description' => $args['description'] ?? '',
            'color'       => $args['color']
        ]);

        return $note;
    }
}