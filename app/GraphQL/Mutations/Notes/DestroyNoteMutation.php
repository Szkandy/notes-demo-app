<?php

namespace App\GraphQL\Mutations\Notes;


use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DestroyNoteMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'updateNote'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name'  => 'id',
                'type'  => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],

        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        $user = auth()->user();
        $user->notes()->find($args['id'])->delete();

        return 'ok';
    }
}