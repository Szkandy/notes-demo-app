<?php

namespace App\GraphQL\Mutations\Notes;


use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class PatchNotesOrderMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'patchNotesOrder'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'ids' => [
                'name'  => 'ids',
                'type'  => Type::nonNull(Type::listOf(Type::int())),
                'rules' => ['required'],
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        /** @var User $user */
        $user = auth()->user();
        $notes = $user->notes()->get()->keyBy('id');

        foreach ($args['ids'] as $position => $id) {
            $notes[$id]->update(['position' => $position]);
        }

        return 'ok';
    }
}