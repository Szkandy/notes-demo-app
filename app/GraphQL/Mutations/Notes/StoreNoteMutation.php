<?php
namespace App\GraphQL\Mutations\Notes;


use Rebing\GraphQL\Support\Mutation;

class StoreNoteMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'storeNote'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return \Graphql::type('note');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        $user = auth()->user();
        return $user->notes()->create(['title' => '', 'description' => '', 'position' => 0, 'color' => 'ivory']);
    }
}