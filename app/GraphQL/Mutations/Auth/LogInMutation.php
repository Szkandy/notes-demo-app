<?php
namespace App\GraphQL\Mutations\Auth;


use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class LogInMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'logIn'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        $user = User::where('name', $args['name'])->first();

        if (!$user) {
            throw new \Exception(trans('auth.failed'));
        }

        $token = auth()->login($user);
        return $token;
    }
}