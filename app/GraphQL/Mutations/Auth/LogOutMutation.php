<?php
namespace App\GraphQL\Mutations\Auth;


use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class LogOutMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'logOut'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        auth()->logout();
        return 'Logged out';
    }
}