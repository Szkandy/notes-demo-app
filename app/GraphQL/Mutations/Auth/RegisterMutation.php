<?php

namespace App\GraphQL\Mutations\Auth;


use App\Models\User;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Mutation;

class RegisterMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'register'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType|null
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [];
    }

    /**
     * @param $root
     * @param $args
     * @return string
     * @throws \Exception
     */
    public function resolve($root, $args)
    {
        $user = User::create(['name' => Str::uuid()]);

        $token = auth()->login($user);
        return $token;
    }
}