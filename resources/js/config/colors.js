let colors = [
    {
        name: "Azure",
        color: "azure"
    },
    {
        name: "Ghost White",
        color: "ghostwhite"
    },
    {
        name: "Ivory",
        color: "ivory"
    },
    {
        name: "Lavender",
        color: "lavender"
    },
    {
        name: "Light Green",
        color: "lightgreen"
    },
    {
        name: "Light Steel Blue ",
        color: "lightsteelblue "
    },
    {
        name: "Peach Puff",
        color: "peachpuff"
    },
    {
        name: "Pink",
        color: "pink"
    },
];


export default colors;