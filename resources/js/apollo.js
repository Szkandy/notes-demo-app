import Vue from 'vue';
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'

import AuthStore from './stores/AuthStore';

const publicLink = new HttpLink({
    uri: Laravel.graphqlEndpoint
});

const appLink = new HttpLink({
    uri: Laravel.graphqlEndpoint + "/app"
});

// Set up Apollo Link with authentication token
const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = AuthStore.getToken();
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            accept: 'application/json',
            authorization: token ? `Bearer ${token}` : "",
        }
    }
});

// Create the apollo client
const publicClient = new ApolloClient({
    link: authLink.concat(publicLink),
    cache: new InMemoryCache(),
    connectToDevTools: true,
});

// Create the apollo client
const apolloAppClient = new ApolloClient({
    link: authLink.concat(appLink),
    cache: new InMemoryCache(),
    connectToDevTools: true,
});


const apolloProvider = new VueApollo({
    clients: {
        default: publicClient,
        app: apolloAppClient
    },
    defaultClient: publicClient,
});

// Install the vue plugin
Vue.use(VueApollo);

export default apolloProvider;