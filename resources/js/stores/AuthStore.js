const AUTH_TOKEN = 'AUTH_TOKEN';

let store = {
    state: {
        isLoggedIn: false
    },
    loginUser(token) {
        this.state.isLoggedIn = true;
        localStorage.setItem(AUTH_TOKEN, token);
    },
    logoutUser() {
        this.state.isLoggedIn = false;
        localStorage.removeItem(AUTH_TOKEN);
    },
    getToken() {
        return localStorage.getItem(AUTH_TOKEN);
    },
    initialize() {
        if (this.getToken()) {
            this.state.isLoggedIn = true;
        }
    }
};

export default store;