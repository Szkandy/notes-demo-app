## About project

This simple app was created as demonstration.

Following functionality was implemented:

- Simple authentication (using **tymon/jwt-auth**)
    - Upon entering application new User is created with UUID. All created notes are then associated with this user.
    - Generated UUID can be used later on to login back.
- GraphQL API
    - All communication between client and server is done via GraphQL
    - Backend is supported by **rebing/graphql-laravel** package
    - Frontend is supported by **apollo-client** iplementation for vue.js
- Notes administration
    - After entering the app, user can create, update, delete and reorder notes
    - Colors for notes are predefined and can be found in **resources/js/config/colors.js** file
 
## Technologies used

- Laravel 5.7
- Vue.js 2.5
- Bootstrap 4
- GraphQL & Apollo client